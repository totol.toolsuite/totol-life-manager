# Retour Toto

## Carte formulaires

### Titre
* FOLDER / NOTES => Ajout un dossier / Ajouter une note 
* Titre de la page + Titre de la carte

### Boutons de validation
* Nom de l'action en toute lettre

### Validation champs
* Clean field : Useless
* Mettre la possibilité "AUCUN" sur le parent folder

## Couleurs
* Ne met de la couleur que si ça a du sens
* Choix des couleurs pour chaque dossier

## Sidebar
* La suppression des dossiers est très ponctuelle, pas besoin de mettre une poubelle a chaque fois
* Prevoir un mode suppression, qui permet meme de supprimer les notes
* Carte de la side bar blanche
* Petit encart de couleur possible à choisir
* Chevron sur la droite (Les gens sont droitiers, ils veulent cliquer avec le pouce
* Couleur pour les notes : Une couleur faible associée à la couleur forte du dossier
* Couleur de plus en plus faible avec la profondeur du dossier
* L'utilisateur choisit la couleur mềre, et subit les couleurs filles

* Suppression : L'utilisateur clique sur une poubelle a téco du titre.
* Une case vide apparait a coté de chaque note / dossier, le titre se change en "supprimer les notes", 
et la poubelle se change en bouton de validation

## Annexe 
* Laisser la possibilité de faire des notes orpheline
* Note non affilié : Pas de couleur, et possible d'attribuer par la suite
* L'écran principal devrait être la liste des notes
* L'utilisateur arrive directement sur la dernière page consultée
* Le burger menu contient uniquement les dossiers racines
* Un nouvel écran arrive avec les niveau 2 et 3 

### Show note
* Liste de notes pas dans des cartes, pas besoin d'etre dans un niveau de profondeur en plus
* La note une fois créée est a plat, elle prend tout l'écran et n'est plus dans une carte
* Basculer les notes terminées dans une catégorie notes terminées
