import { Component, OnInit } from '@angular/core';
import { note } from 'src/app/models/note.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DatabaseService } from 'src/app/services/database.service';
import { MenuController, ToastController, AlertController, ModalController } from '@ionic/angular';
import { folder } from 'src/app/models/folder.model';
import _ from 'lodash';

@Component({
  selector: 'app-show',
  templateUrl: './show.page.html',
  styleUrls: ['./show.page.scss'],
})
export class ShowPage implements OnInit {
  loadedNote: note = new note();
  folderRoute: String;
  noteId: String;
  root: String;
  nest1: String;
  nest2: String;
  nest3: String;
  loadedImage: string;
  doneItemsNumber: number;
  todoItemsNumber: number;
  parentFolder: folder;
  loaded: boolean = false;

  constructor(
    public alertController: AlertController,
    private menu: MenuController,
    private router: Router,
    private activatedRoute: ActivatedRoute, 
    private db: DatabaseService,
    private toastController: ToastController
  ) { }

  ngOnInit() {
    this.init();
  }
  async init() {
    this.activatedRoute.paramMap.subscribe(async paramMap => {
      if (!paramMap.has('noteId')) {
        this.router.navigate(['/home']); 
        return;
      }
      this.noteId = paramMap.get('noteId');
      this.db.getNote(this.noteId).then(note => {
        if (note) {
          this.loadedNote = note;
          this.db.getFolder(note.folder).then(folder => {
            this.parentFolder = folder
          })
          this.setNumberOfDoneItems();
        }
        this.loaded=true;
      })

      this.root = paramMap.get('root');
      this.nest1 = paramMap.get('nest1');
      this.nest2 = paramMap.get('nest2');
      this.nest3 = paramMap.get('nest3');

      this.concatRoute(this.root, this.nest1, this.nest2, this.nest3);
    });
    
    if (this.menu.isOpen()) {
      this.menu.close();
    }
  }

  async deleteNote() {
    const alert = await this.alertController.create({
      header: 'Are you sure?!',
      message: 'This operation <strong>can\'t be undone</strong>!!!',
      cssClass: 'modal-css',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: async () => {
            this.db.deleteNote(this.loadedNote.id);
            this.router.navigate(['/list']); 
            const toast = await this.toastController.create({
              message: 'Note deleted.',
              duration: 1000
            });
            toast.present();
          }
        }
      ]
    });
    await alert.present();
  }

  openMenu() {
    this.menu.enable(true, 'first');
    this.menu.open();
  }

  concatRoute(root: String, nest1: String, nest2: String, nest3: String) {
    if (this.loadedNote && root) {
      this.folderRoute = root;
      if (nest1) {
        this.folderRoute += ' > ' + nest1;
        if (nest2) {
          this.folderRoute += ' > ' + nest2;
          if (nest3) {
            this.folderRoute += ' > ' + nest3;
          }
        }
      }
    }
  }

  setNumberOfDoneItems() {
    let num = 0
    if (this.loadedNote && this.loadedNote.listItems && this.loadedNote.listItems.length > 0) {
      for (let item of this.loadedNote.listItems) {
        if (item.done == true) {
          num ++;
        }
      }
      this.doneItemsNumber = num;
    }
    this.todoItemsNumber = this.loadedNote.listItems.length - this.doneItemsNumber
  }

  getDoneItems() {
    return _.filter(this.loadedNote.listItems, x => x.done == true);
  }

  getTodoItems() {
    return _.filter(this.loadedNote.listItems, x => x.done == false);
  }

  check(item, value) {
    let index = _.findIndex(this.loadedNote.listItems, x => x.item == item.item)
    this.loadedNote.listItems[index].done = value;
    this.setNumberOfDoneItems();
    this.db.checkItemNote(this.noteId, JSON.stringify(this.loadedNote.listItems));
  }

  truncateString(str, num) {
    if (!str || str.length <= num) {
      return str
    }
    return str.slice(0, num) + '...'
  }
  
  test() {
    console.log('testestse')
  }
}
