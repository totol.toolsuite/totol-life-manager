import { Component, OnInit } from '@angular/core';
import { FolderService } from 'src/app/services/folder.service';
import { DatabaseService } from 'src/app/services/database.service';
import { Observable } from 'rxjs';
import { note } from 'src/app/models/note.model';
import { folder } from 'src/app/models/folder.model';
import _ from 'lodash';
import { MenuController, ToastController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  loaded: boolean = false;
  foldersToDelete: folder[] = [];
  notesToDelete: note[] = [];
  currentFolder: folder; 
  deleteMode: boolean = false;
  folders: folder[] = [];
  notes: note[] = [];
  constructor(
    private alertController: AlertController,
    private toastController: ToastController,
    private menu: MenuController,
    private router: Router,
    private activatedRoute: ActivatedRoute, 
    private db: DatabaseService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(async paramMap => {
      if (paramMap.has('folderId')) {
        this.db.getFolder(paramMap.get('folderId')).then(async data => {
          await this.db.loadFolders();
          await this.db.loadNotes();
          this.db.observeFolders().subscribe(folders => {
            this.folders = folders;
            this.db.observeNotes().subscribe(notes => {
              this.notes = notes;
              this.currentFolder = data;
              this.constructFolderObject();
              if (!paramMap.has('folderId') || !this.currentFolder) {
                this.menu.open();
              }
              this.loaded=true
            })
          })
        });
        // this.currentFolder = this.folderController.getCurrentFolder();
      }
    });
    this.menu.close();
  }

  openMenu() {
    this.menu.enable(true, 'first');
    this.menu.open();
  }

  truncateString(str, num) {
    if (!str || str.length <= num) {
      return str
    }
    return str.slice(0, num) + '...'
  }

  deployFolder(folder: folder) {
    folder.isDeployed = !folder.isDeployed;
  }

  setDeleteModeOn() {
    this.deleteMode = true;
  }

  async validateDelete() {
    const alert = await this.alertController.create({
      header: 'Are you sure?!',
      message: 'Child of selected folders <strong>will be deleted too</strong>!!!',
      cssClass: 'modal-css',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: async () => {
            for (let folder of this.foldersToDelete) {
              await this.db.deleteFolder(folder.id);
            }
            for (let note of this.notesToDelete) {
              await this.db.deleteNote(note.id);
            }
            this.deleteMode = false;
            const toast = await this.toastController.create({
              message: `${this.foldersToDelete.length} folders and ${this.notesToDelete.length} notes deleted`,
              duration: 1000
            });
            toast.present();
          }
        }
      ]
    });
    await alert.present();

  }

  addFolderToDeleteList(folder) {
    folder.onDelete = true;
    this.foldersToDelete.push(folder);
  }

  removeFolderFromDeleteList(folder) {
    let folderIndex = _.findIndex(this.foldersToDelete, x => x.id == folder.id);
    this.foldersToDelete.splice(folderIndex, 1);
    folder.onDelete = false;
  }

  addNoteToDeleteList(note) {
    note.onDelete = true;
    this.notesToDelete.push(note);
  }

  removeNoteFromDeleteList(note) {
    let noteIndex = _.findIndex(this.notesToDelete, x => x.id == note.id);
    this.notesToDelete.splice(noteIndex, 1);
    note.onDelete = false;
  }

  constructFolderObject() {
    let tempCurrentFolder = _.cloneDeep(this.currentFolder);
    tempCurrentFolder.notes =_.filter(this.notes, x => x.folder == this.currentFolder.id);
    let folderLevelOne = _.filter(this.folders, x => x.parentFolder == this.currentFolder.id);
    for (let folder1 of folderLevelOne) {
      folder1.notes = _.filter(this.notes, x => x.folder == folder1.id);
      let folderLevelTwo = _.filter(this.folders, x => x.parentFolder == folder1.id);
      for (let folder2 of folderLevelTwo) {
        folder2.notes = _.filter(this.notes, x => x.folder == folder2.id);
      }
      folder1.folders = folderLevelTwo;
    }
    tempCurrentFolder.folders = folderLevelOne;
    this.currentFolder = tempCurrentFolder;
  }

  getNbItemsDone(note) {
    return _.filter(note.listItems, x => x.done).length;
  }
}
