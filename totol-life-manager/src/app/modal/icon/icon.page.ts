import { Component, OnInit } from '@angular/core';
import { FolderService } from 'src/app/services/folder.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.page.html',
  styleUrls: ['./icon.page.scss'],
})
export class IconPage implements OnInit {
  iconArray: string[] = [];
  constructor(private folderService: FolderService, private modalController: ModalController) { }

  ngOnInit() {
    this.iconArray = this.folderService.getIconArray();
  }

  selectIcon(iconName: string) {
    this.folderService.selectIcon(iconName);
    this.dismiss();
  }


  quitModal() {
    this.dismiss();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
