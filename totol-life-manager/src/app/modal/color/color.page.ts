import { Component, OnInit } from '@angular/core';
import { ColorService } from '../../services/color.service'
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-color',
  templateUrl: './color.page.html',
  styleUrls: ['./color.page.scss'],
})
export class ColorPage implements OnInit {
  colorArray: string[] = [];
  constructor(
    private colorService: ColorService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.colorArray = this.colorService.getColorArray();
  }

  selectColor(color: string) {
    this.colorService.setColor(color);
    this.dismiss();
  }


  quitModal() {
    this.dismiss();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
