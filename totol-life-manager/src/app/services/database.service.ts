import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';
import { folder } from '../models/folder.model';
import { note } from '../models/note.model';
import { browserDBInstance } from './browser';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { listItem } from '../models/list-items';

declare var window: any;
const SQL_DB_NAME = '_native.db';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  dbInstance: any;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  folders = new BehaviorSubject([]);
  notes = new BehaviorSubject([]);
 
  //_______Web dev mode start________

  // constructor(
  //   private plt: Platform, 
  //   public sqlite: SQLite) {
  //   this.init();
  // }

  // async init() {
  //   let db = window.openDatabase(SQL_DB_NAME, '1.0', 'DEV', 5 * 1024 * 1024);
  //   this.dbInstance = browserDBInstance(db);
  //   this.createTablesIfNotExist();
  //   this.execNoteTrigger();
  //   this.execFolderTrigger();
  // }

  // async createTablesIfNotExist() {
  //   // await this.dbInstance.executeSql('DROP TABLE folder');
  //   // await this.dbInstance.executeSql('DROP TABLE note');
  //   await this.dbInstance.executeSql('CREATE TABLE IF NOT EXISTS folder' +
  //     '(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, icon TEXT, deep INTEGER, folderId INTEGER, color TEXT);');
      
  //   await this.dbInstance.executeSql(
  //     'CREATE TABLE IF NOT EXISTS note' +
  //     '(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, content TEXT, imageUrl TEXT, folderId INTEGER, listItems TEXT);');
  //   await this.loadFolders();
  //   await this.loadNotes();
  //   this.dbReady.next(true);
  // }


  // addFolder(name, icon, parent, deep, color) {
  //   let data = [name, icon, parent, deep, color];
  //   if (parent != null) {
  //     let query = `INSERT or IGNORE INTO folder(name, icon, folderId, deep, color) VALUES(\'${data[0]}\', \'${data[1]}\', \'${data[2]}\', \'${data[3]}\', \'${data[4]}\');`
  //     return this.dbInstance.executeSql(query)
  //     .then(data => {
  //       this.loadFolders();
  //       return data.rowsAffected;
  //     })
  //   } else {
  //     let query = `INSERT or IGNORE INTO folder (name, icon, deep, color) VALUES (\'${data[0]}\', \'${data[1]}\', \'${data[3]}\', \'${data[4]}\');`
  //     return this.dbInstance.executeSql(query)
  //     .then(data => {
  //       this.loadFolders();
  //       return data.rowsAffected;
  //     })
  //   }
  // }

   
  // addNote(title, content, folder, image, listItems) {
  //   let data = [title, content, folder != -1 ? folder : '', image, listItems != '[]' ? listItems : ''];
  //   let query = `INSERT INTO note(title, content, folderId, imageUrl, listItems) VALUES (\'${data[0]}\', \'${data[1]}\', \'${data[2]}\', \'${data[3]}\', \'${data[4]}\');`
  //   console.log(query)
  //   return this.dbInstance.executeSql(query)
  //   .then(data => {
  //     this.loadNotes();
  //     return data.rowsAffected;
  //   }).catch(eror => {
  //     console.log('Error', JSON.stringify(eror))
  //   })
  // }
  //________Web dev mode end_________


 
  //________Mobile mode start________
  constructor(private plt: Platform, private sqlitePorter: SQLitePorter, private sqlite: SQLite, private http: HttpClient) {
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'developers.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
          this.dbInstance = db;
          this.seedDatabase();
      });
    });
  }
 
  seedDatabase() {
    this.http.get('assets/seed.sql', { responseType: 'text'})
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.dbInstance, sql)
        .then(_ => {
          this.loadFolders();
          this.loadNotes();
          this.execNoteTrigger();
          this.execFolderTrigger();
          this.dbReady.next(true);
        })
        .catch(e => console.error(e));
    });
  }

  addFolder(name, icon, parent, deep, color) {
    let data = [name, icon, parent, deep, color];
    return this.dbInstance.executeSql('INSERT or IGNORE INTO folder(name, icon, folderId, deep, color) VALUES (?, ?, ?, ?, ?)', data).then(data => {
      console.log('ADD FOLDER : SUCCESS')
      this.loadFolders();
      return data.rowsAffected;
    }).catch(eror => {
      console.log('Error', JSON.stringify(eror))
    })
  }

  addNote(title, content, folder, image, listItems) {
    let data = [title, content, folder != -1 ? folder : '', image, listItems != '[]' ? listItems : ''];
    return this.dbInstance.executeSql('INSERT INTO note(title, content, folderId, imageUrl, listItems) VALUES (?, ?, ?, ?, ?)', data).then(data => {
      console.log('ADD NOTE : SUCCESS')
      this.loadNotes();
      return data.rowsAffected;
    }).catch(eror => {
      console.log('Error', JSON.stringify(eror))
    })
  }
 
  // //________Mobile mode end___________


  execNoteTrigger() {
    let query = "CREATE TRIGGER IF NOT EXISTS note_cascade " +
    "AFTER DELETE "+
    "ON folder "+
    "FOR EACH ROW "+
    "BEGIN "+
    "DELETE FROM note WHERE note.folderId = OLD.id; "+
    "END";
    return this.dbInstance.executeSql(query, []).then(data => {
      console.log('TRIGGER : OK')
    }).catch(error => {
      console.log(JSON.stringify(error));
    })
  }

  execFolderTrigger() {
    let query = "CREATE TRIGGER IF NOT EXISTS folder_cascade " +
    "AFTER DELETE "+
    "ON folder "+
    "FOR EACH ROW "+
    "BEGIN "+
    "DELETE FROM folder WHERE folder.folderId = OLD.id; "+
    "END";
    return this.dbInstance.executeSql(query, []).then(data => {
      console.log('TRIGGER : OK')
    }).catch(error => {
      console.log(JSON.stringify(error));
    })
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }

  loadFolders() {
    let query = 'SELECT * FROM folder;';

    return this.dbInstance.executeSql(query, []).then(data => {
      let folders: folder[] = [];
 
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let currFolder: folder = new folder();
          currFolder.id = data.rows.item(i).id
          currFolder.name = data.rows.item(i).name
          currFolder.icon = data.rows.item(i).icon
          currFolder.parentFolder = data.rows.item(i).folderId
          currFolder.deep = data.rows.item(i).deep
          currFolder.color = data.rows.item(i).color
          folders.push(currFolder);
        }
      }
      this.folders.next(folders);
    })
  }
 
  deleteFolder(id) {
    return this.dbInstance.executeSql(`DELETE FROM folder WHERE id = ?;`, [id]).then(_ => {
      this.loadNotes();
      this.loadFolders();
    }).catch(eror => {
      console.log('Error', JSON.stringify(eror))
    })
  }

  deleteNote(id) {
    return this.dbInstance.executeSql(`DELETE FROM note WHERE id = ?;`, [id]).then(_ => {
      this.loadNotes();
      this.loadFolders();
    }).catch(eror => {
      console.log('Error', JSON.stringify(eror))
    })
  }
 
  updateFolder(folder: folder) {
    let data = [folder.name, folder.icon];
    return this.dbInstance.executeSql(`UPDATE folder SET name = ?, icon = ? WHERE id = ${folder.id}`, data).then(data => {
      this.loadFolders();
    })
  }

  async updateNote(note: note, listItems: string, imageUrl: string) {
    let imageQuery = (imageUrl != 'undefined') ? `, imageUrl = \'${imageUrl}\'` : '';
    let listQuery = (listItems != '[]') ? `, listItems = \'${listItems}\'` : '';
    let query = `
    UPDATE note 
    SET title = \'${note.title}\',
    content = \'${note.content}\' ${imageQuery} ${listQuery}
    WHERE id = ${note.id}`;
    return this.dbInstance.executeSql(query
    ).then(data => {
      this.loadNotes();
    }).catch(eror => {
      console.log('Error', JSON.stringify(eror))
    })
  }
 
  addImageToNote(id: String, imageUrl: String) {
    return this.dbInstance.executeSql(`UPDATE note SET imageUrl = \'${imageUrl}\' WHERE id = ${id}`).then(data => {
      this.loadNotes();
    }).catch(eror => {
      console.log('Error', JSON.stringify(eror))
    })
  }

  checkItemNote(id: String, listItems: String) {
    return this.dbInstance.executeSql(`UPDATE note SET listItems = \'${listItems}\' WHERE id = ${id}`).then(data => {
      this.loadNotes();
    }).catch(eror => {
      console.log('Error', JSON.stringify(eror))
    })
  }

  loadNotes() {
    let query = 'SELECT note.title, note.content, note.id, note.listItems, folder.id AS folder FROM note LEFT JOIN folder ON folder.id = note.folderId';
    return this.dbInstance.executeSql(query, []).then(data => {
      let notes = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let listItems = [];
          if (data.rows.item(i) && data.rows.item(i).listItems) {
            listItems = JSON.parse(data.rows.item(i).listItems);
          }
          notes.push({ 
            id: data.rows.item(i).id,
            title: data.rows.item(i).title,
            content: data.rows.item(i).content,
            folder: data.rows.item(i).folder,
            imageUrl: data.rows.item(i).imageUrl,
            listItems: listItems
           });
        }
      }
      this.notes.next(notes);
    });
  }

  getNote(id): Promise<note> {
    return this.dbInstance.executeSql(`SELECT * FROM note WHERE id = ?`, [id]).then(data => { 
      let listItems = [];
      if (data.rows.item(0) && data.rows.item(0).listItems) {
        listItems = JSON.parse(data.rows.item(0).listItems);
      }
      let currNote: note = new note();
      currNote.id = data.rows.item(0).id;
      currNote.title = data.rows.item(0).title;
      currNote.content = data.rows.item(0).content;
      currNote.folder = data.rows.item(0).folderId ? data.rows.item(0).folderId : '';
      currNote.imageUrl = data.rows.item(0).imageUrl ? data.rows.item(0).imageUrl : '';
      currNote.listItems = listItems;
      return currNote;
    }).catch(eror => {
      console.log('Error', JSON.stringify(eror))
    })
  }

  getFolder(id): Promise<folder> {
    return this.dbInstance.executeSql(`SELECT * FROM folder WHERE id = ?`, [id]).then(data => { 
      let currFolder: folder = new folder();
      currFolder.id = data.rows.item(0).id
      currFolder.name = data.rows.item(0).name
      currFolder.icon = data.rows.item(0).icon
      currFolder.deep = data.rows.item(0).deep
      currFolder.color = data.rows.item(0).color
      return currFolder;
    }).catch(eror => {
      console.log('Error', JSON.stringify(eror))
    })
  }

  observeFolders(): Observable<folder[]> {
    return this.folders.asObservable();
  }

  observeNotes(): Observable<note[]> {
    return this.notes.asObservable();
  }
}