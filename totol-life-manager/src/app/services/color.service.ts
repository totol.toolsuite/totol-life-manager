import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  private color: string = '#ffffff';
  private colorArray = [
    '#66ffff',
    '#ffccff',
    '#ffcc99',
    '#ccffcc',
    '#99ccff',
    '#ff99ff',
    '#ff9999',
    '#66ff33',
    '#0066ff',
    '#cc00ff',
    '#ff0066',
    '#669999',
    '#000066',
    '#660066',
    '#660033',
    '#666633',
    '#000000',
    '#737373',
    '#d9d9d9',
    '#ffffff'
  ]

  constructor() { }

  public getColorArray() {
    return this.colorArray;
  }

  public getColor() {
    return this.color;
  }

  public setColor(color) {
    this.color = color;
  }

  public setColorDefault() {
    this.color = '';
  }
}
