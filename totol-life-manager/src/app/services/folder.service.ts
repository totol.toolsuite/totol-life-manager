import { Injectable } from '@angular/core';
import { folder } from '../models/folder.model';

@Injectable({
  providedIn: 'root'
})
export class FolderService {
  private currentFolder: folder;
  private iconArray: string[] = 
  [
    'add-circle-outline', 
    'airplane-outline',
    'alarm-outline',
    'at-outline',
    'alert-circle-outline',
    'beer-outline',
    'bicycle-outline',
    'bug-outline',
    'build-outline',
    'car-outline',
    'color-palette-outline',
    'battery-half-outline',
    'body-outline',
    'calendar-outline',
    'earth-outline',
    'hammer-outline',
    'location-outline',
    'planet-outline',
    'pizza-outline',
    'snow-outline',
    'thermometer-outline',
    'water-outline',
    'train-outline',
    'stats-chart-outline',
    'checkbox-outline',
    'ear-outline',
    'fast-food-outline',
    'football-outline',
    'game-controller-outline',
    'gift-outline',
    'headset-outline',
    'medal-outline',
    'rainy-outline',
    'rocket-outline',
    'skull-outline',
    'trophy-outline'
  ];
  private selectedIcon: string = 'add-circle-outline';
  constructor() { }

  public selectIcon(iconName: string) {
    this.selectedIcon = iconName;
  }

  public getIconArray () {
    return [...this.iconArray];
  }

  public getSelectedIcon () {
    return this.selectedIcon;
  }

  public setCurrentFolder (folder) {
    this.currentFolder = folder;
  }

  public getCurrentFolder () {
    return this.currentFolder;
  }
}
