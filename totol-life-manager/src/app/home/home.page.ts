import { Component } from '@angular/core';
import { ModalController, MenuController, ToastController } from '@ionic/angular';
import { IconPage } from '../modal/icon/icon.page';
import { FolderService } from '../services/folder.service';
import { DatabaseService } from '../services/database.service';
import { folder } from 'src/app/models/folder.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ImagePicker, ImagePickerOptions, OutputType } from '@ionic-native/image-picker/ngx';
import { Capacitor } from '@capacitor/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import _ from 'lodash';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  folders: folder[];
  currentNote: String;
  parentFolder: folder;
  listItems: string = '';
  loadedImage: string = 'undefined';
  selected: string = 'folder'; 
  iconFolder: string = 'add-circle-outline';
  titleFolder: string = '';
  titleNote: string = '';
  noteFolder: number = -1;
  contentNote: string = '';

  errors = {
    titleNote: false,
    titleNote2: false,
    titleFolder: false,
    titleFolder2: false,
    iconFolder: false,
    contentNote: false,
    noteFolder: false
  }
  errorMessages = {
    titleNote: 'This field must be between 1 and 20 chars',
    titleNote2: 'The note couldn\'t be created, maybe another folder exist with same name',
    titleFolder: 'This field must be between 1 and 20 chars',
    titleFolder2: 'The folder couldn\'t be created, maybe another folder exist with same name',
    iconFolder: 'Select an icon for the folder',
    contentNote: 'This field must be between 1 and 500 chars',
    noteFolder: 'Select a folder for the note'
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute, 
    private camera: Camera,
    private imagePicker: ImagePicker,
    private menu: MenuController,
    private modalController: ModalController, 
    private folderController: FolderService,
    private db: DatabaseService,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has('tab')) {
        this.router.navigate(['/home/folder']); 
        return;
      }
      this.selected = paramMap.get('tab');
    });
    this.db.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.db.observeFolders().subscribe(folders => {
          this.folders = folders;
        })
      } else {
      }
    })
  }

  async presentIconModal() {
    const modal = await this.modalController.create({
      component: IconPage
    });
    return await modal.present();
  }

  get icon() {
    this.iconFolder = this.folderController.getSelectedIcon()
    return this.iconFolder
  }

  clearForm(form: string) {
    if (form === 'note') {
      this.titleNote = '';
      this.contentNote = '';
      this.loadedImage = 'undefined'
    } else {
      this.titleFolder = '';
      this.folderController.selectIcon('add-circle-outline');
      this.parentFolder = null;
    }
  }


  selectForm(form: string) {
    this.selected = form;
    this.clearForm(form);
  }

  openMenu() {
    this.menu.enable(true, 'first');
    this.menu.open();
  }
  
  getFolderDepsMax(deps) {
    return _.filter(this.folders, x => x.deep < deps);
  }

  validateTitleNote() {
    this.errors.titleNote2 = false;
    this.errors.titleNote = (this.titleNote.length < 1 || this.titleNote.length > 20);
  }

  validateContentNote() {
    this.errors.contentNote = (this.contentNote.length < 1 || this.contentNote.length > 500);
  }

  validatenoteFolder() {
    this.errors.noteFolder = (this.noteFolder < 0);
  }

  validateTitleFolder() {
    this.errors.titleFolder2 = false;
    this.errors.titleFolder = (this.titleFolder.length < 1 || this.titleFolder.length > 20);
  }

  validateIconFolder() {
    this.errors.iconFolder = (this.iconFolder.length < 1);
  }
  
  async addImage() {
    let options: ImagePickerOptions = {
      maximumImagesCount: 1,
      outputType: OutputType.FILE_URL,
      quality: 100,
    }
    await this.imagePicker.getPictures(options).then((results) => {
      this.loadedImage = results.length > 0 ? Capacitor.convertFileSrc(results[0]) : 'undefined';
    }, (err) => { 
      console.log('error picker', err)
     });
  }

  useCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.loadedImage = imageData ? Capacitor.convertFileSrc(imageData) : 'undefined';
    //  let base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log('error camera', err)
    });
  }
}
