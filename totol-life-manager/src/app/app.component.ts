import { Component, OnInit } from '@angular/core';

import { Platform, MenuController, ToastController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DatabaseService } from './services/database.service';
import { FolderService } from './services/folder.service';
import { folder } from './models/folder.model';
import { note } from './models/note.model';
import _ from 'lodash';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit{
  foldersWorkedFiltered: folder [];
  foldersWorked: folder[];
  folders: folder[];
  notes: note[];
  orphanNotesFiltered: note[];
  orphanNotes: note[];
  deleteMode: boolean = false;
  filter: string = '';

  constructor(
    public alertController: AlertController,
    private menu: MenuController,
    private router: Router,
    private db: DatabaseService,
    private folderController: FolderService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private toastController: ToastController
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.db.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.db.observeFolders().subscribe(folders => {
          this.folders = folders;
          this.fitNotesInFolders();
          this.fitFoldersInFolders();
          this.triggerFilter()
        })
        this.db.observeNotes().subscribe(notes => {
          this.notes = notes;
          this.fitNotesInFolders();
          this.fitFoldersInFolders();
          this.triggerFilter()
        })
        this.fitNotesInFolders();
        this.fitFoldersInFolders();
        this.cleanMovedFolders();
        this.triggerFilter()
        this.splashScreen.hide();
      } else {
      }
    })
  }

  onMenuOpen() {
    this.fitNotesInFolders();
    this.fitFoldersInFolders();
    this.cleanMovedFolders();
  }

  fitNotesInFolders() {
    if (this.notes) {
      this.foldersWorked = _.cloneDeep(this.folders);
      for(let note of this.notes) {
        let folderIndex = _.findIndex(this.folders, x => x.id == note.folder);
        if (folderIndex > 0) {
          this.foldersWorked[folderIndex].notes.push(note);
        }
      }
      this.orphanNotes = _.filter(this.notes, x => !x.folder)
    }
  }

  fitFoldersInFolders() {
    if (!this.foldersWorked) {
      return;
    }
    let maxDeep = _.maxBy(this.foldersWorked, 'deep');
    if (!maxDeep || maxDeep.deep < 1) {
      return;
    }
    let filteredByDeepFolders = _.filter(this.foldersWorked, x => x.deep === maxDeep.deep);
    for (let folder of filteredByDeepFolders) {
      let folderIndex = _.findIndex(this.foldersWorked, x => x.id == folder.parentFolder);
      folder.toDelete = true;
      if (folderIndex >= 0) {
        if (!this.foldersWorked[folderIndex].folders.includes(folder)) {
          this.foldersWorked[folderIndex].folders.push(folder);
        }
      } else {
        this.deleteFolder(folder.id);
      }
    }
    this.cleanMovedFolders();
    this.fitFoldersInFolders();
  }

  cleanMovedFolders() {
    if (this.foldersWorked) {
      this.foldersWorked.forEach((folder, pos) => {
        if (folder.toDelete) {
          this.foldersWorked.splice(pos, 1);
        }
    });
    }
  }

  async deleteFolder(folderId) {
    await this.db.deleteFolder(folderId);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
    });
  }

  deployFolder(folder: folder) {
    folder.isDeployed = !folder.isDeployed;
  }

  truncateString(str, num) {
    if (!str || str.length <= num) {
      return str
    }
    return str.slice(0, num) + '...'
  }

  async showFolderList(folder) {
    await this.folderController.setCurrentFolder(folder);
    if (this.router.url == '/list/' + this.folderController.getCurrentFolder().id) {
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate(['/list', folder.id])); 
    } else {
      this.router.navigate(['/list', folder.id]); 
    }
    this.menu.close();
  }
  
  goToNote(id) {
    this.router.navigate(['/show', id]); 
  }

  setDeleteModeOn() {
    this.deleteMode = true;
  }

  async validateDelete() {
    const alert = await this.alertController.create({
      header: 'Are you sure?!',
      message: 'This operation <strong>can\'t be undone</strong>!!!',
      cssClass: 'modal-css',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: async () => {
            let foldersToDelete = _.filter(this.foldersWorked, x => x.onDelete == true);
            let noteToDelete = _.filter(this.orphanNotes, x => x.onDelete == true);
            for (let folder of foldersToDelete) {
              await this.db.deleteFolder(folder.id);
              this.triggerFilter();
            }
            for (let note of noteToDelete) {
              await this.db.deleteNote(note.id);
              this.triggerFilter();
            }
            this.foldersWorked = this.foldersWorked.map(x => {x.onDelete = false; return x;});
            this.orphanNotes = this.orphanNotes.map(x => {x.onDelete = false; return x;});
            this.deleteMode = false;
            this.fitNotesInFolders();
            this.fitFoldersInFolders();
            this.cleanMovedFolders();

            const toast = await this.toastController.create({
              message: `${foldersToDelete.length} folders and ${noteToDelete.length} notes deleted`,
              duration: 1000
            });
            toast.present();
          }
        }
      ]
    });
    await alert.present();
    this.triggerFilter();

  }

  setOnDelete(folderOrNote, bool) {
    folderOrNote.onDelete = bool;
  }

  triggerFilter() {
    this.foldersWorkedFiltered = _.cloneDeep([]);
    this.orphanNotesFiltered = _.cloneDeep([]);
    requestAnimationFrame(() => {
      this.foldersWorked.forEach(folder => {
        const shouldShow = folder.name.toLowerCase().includes(this.filter.toLowerCase());
        if (shouldShow) {
          if (!this.foldersWorkedFiltered.includes(folder)) {
            this.foldersWorkedFiltered.push(folder);
          }
        }
      });
      this.orphanNotes.forEach(note => {
        const shouldShow = note.title.toLowerCase().includes(this.filter.toLowerCase());
        if (shouldShow) {
          if (!this.orphanNotesFiltered.includes(note)) {
            this.orphanNotesFiltered.push(note);
          }
        }
      });
    });
  }
  getNbItemsDone(note) {
    return _.filter(note.listItems, x => x.done).length;
  }
  closeMenu() {
    this.menu.close();
  }
}
