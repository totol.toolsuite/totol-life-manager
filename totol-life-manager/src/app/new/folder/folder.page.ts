import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FolderService } from 'src/app/services/folder.service';
import { MenuController, ToastController, ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { DatabaseService } from 'src/app/services/database.service';
import { folder } from 'src/app/models/folder.model';
import { IconPage } from 'src/app/modal/icon/icon.page';
import _ from 'lodash';
import { ColorPage } from 'src/app/modal/color/color.page';
import { ColorService } from 'src/app/services/color.service';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  folders: folder[] = [];
  parentFolder: folder;
  loadedFolder: folder = new folder();
  parentId: string;
  loadedImage: string = 'undefined';
  hasParent: boolean = false;
  currentNewItem: string = '';

  errors = {
    titleSize: false,
    titleUnique: false,
    iconLess: false,
  }
  errorMessages = {
    titleSize: 'This field must be between 1 and 20 chars',
    titleUnique: 'The folder couldn\'t be created, maybe another folder exist with same name',
    iconLess: 'Select an icon for the folder',
  }
  constructor(
    private location: Location,
    private toastController: ToastController,
    private folderController: FolderService,
    private menu: MenuController,
    private router: Router,
    private activatedRoute: ActivatedRoute, 
    private db: DatabaseService,
    private modalController: ModalController, 
    private colorController: ColorService
  ) { }

  ngOnInit() {
    this.init();
  }

  async init() {
    this.activatedRoute.paramMap.subscribe(async paramMap => {
      this.parentId = paramMap.get('parentId');
      if (this.parentId ) {
        this.hasParent = true;
        this.db.getFolder(this.parentId).then(folder => {
          this.parentFolder = folder;
          this.loadedFolder.parentFolder = folder;
        })
      } else {
        this.hasParent = false;
      }
      this.db.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.db.observeFolders().subscribe(folders => {
            this.folders = folders;
          })
        } else {
        }
      })
    });
  }

  openMenu() {
    this.menu.enable(true, 'first');
    this.menu.open();
  }

  getFolder3DepsMax(maxDeep: number) {
    let folderChild = [];
    if (this.hasParent) {
      folderChild = _.filter(this.folders, x => x.parentFolder == this.parentId);
      folderChild.push(_.find(this.folders, x => x.id == this.parentId));
    } else {
      folderChild = [{id: -1, name: 'No parent', deep: 0}, ...this.folders];
    }
    return folderChild.length > 0 ?_.filter(folderChild, x => x.deep < maxDeep) : [];
  }


  async presentIconModal() {
    const modal = await this.modalController.create({
      component: IconPage
    });
    return await modal.present();
  }

  async presentColorModal() {
    const modal = await this.modalController.create({
      component: ColorPage
    });
    return await modal.present();
  }

  get color() {
    this.loadedFolder.color = this.colorController.getColor();
    return this.loadedFolder.color;
  }

  get icon() {
    this.loadedFolder.icon = this.folderController.getSelectedIcon();
    return this.loadedFolder.icon;
  }

  validateTitle() {
    this.errors.titleUnique = false;
    this.errors.titleSize = (this.loadedFolder.name.length < 1 || this.loadedFolder.name.length > 20);
  }

  validateIcon() {
    this.errors.iconLess = (this.loadedFolder.icon.length < 1);
  }

  async addFolder() {
    this.validateTitle();
    this.validateIcon();
    if (!this.errors.titleSize && !this.errors.iconLess && !this.errors.titleUnique) {
      this.db.addFolder(
        this.loadedFolder.name, 
        this.loadedFolder.icon, 
        !this.loadedFolder.parentFolder || this.loadedFolder.parentFolder.id < 0 ? null : this.loadedFolder.parentFolder.id, 
        !this.loadedFolder.parentFolder || this.loadedFolder.parentFolder.id < 0 ? 0 :this.loadedFolder.parentFolder.deep + 1,
        this.loadedFolder.color
        ).then(data => {
        if (data == 0) {
          this.errors.titleUnique = true;
        }
      })
      this.loadedFolder.name = '';
      this.loadedFolder.parentFolder = null;
      this.loadedFolder.icon = 'add-circle-outline';
      this.location.back();
      const toast = await this.toastController.create({
        message: 'Folder created.',
        duration: 1000
      });
      toast.present();
    }
  }
}
