import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { DatabaseService } from 'src/app/services/database.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuController, ToastController } from '@ionic/angular';
import { FolderService } from 'src/app/services/folder.service';
import { folder } from 'src/app/models/folder.model';
import { note } from 'src/app/models/note.model';
import { listItem } from '../../models/list-items';
import _ from 'lodash';
import { ImagePicker, ImagePickerOptions, OutputType } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-note',
  templateUrl: './note.page.html',
  styleUrls: ['./note.page.scss'],
})
export class NotePage implements OnInit {
  folders: folder[];
  loadedNote: note = new note();
  listItems: listItem[] = [];
  parentId: string;
  loadedImage: string = 'undefined';
  hasParent: boolean = false;
  isEditing: boolean = false;
  currentNewItem: string = '';
  parentFolder: folder;

  errors = {
    listItemSize: false,
    titleSize: false,
    titleUnique: false,
    contentSize: false,
  }
  errorMessages = {
    listItemSize: 'Item must be between 1 and 150 chars',
    titleSize: 'This field must be between 1 and 20 chars',
    titleUnique: 'The note couldn\'t be created, maybe another folder exist with same name',
    contentSize: 'This field must be between 1 and 500 chars',
  }
  constructor(
    private location: Location,
    private camera: Camera,
    private imagePicker: ImagePicker,
    private toastController: ToastController,
    private folderController: FolderService,
    private menu: MenuController,
    private router: Router,
    private activatedRoute: ActivatedRoute, 
    private db: DatabaseService
  ) { }

  ngOnInit() {
    this.init();
  }

  async init() {
    this.activatedRoute.paramMap.subscribe(async paramMap => {
      this.parentId = paramMap.has('parentId') ? paramMap.get('parentId') : null;
      this.isEditing = this.router.url.includes('edit');

      if (this.isEditing) {
        this.db.getNote(paramMap.get('editedNote')).then(data => {
          this.loadedNote = data;
          this.listItems = this.loadedNote.listItems;
          this.parentId = this.loadedNote.folder.toString();
          this.hasParent = true;
          if (this.loadedNote.imageUrl != '' && this.loadedNote.imageUrl != 'undefined') {
            this.loadedImage = this.loadedNote.imageUrl;
          }
          this.db.getFolder(this.parentId).then(folder => {
            this.loadedNote.folder = folder;
            this.parentFolder = folder;
          })
        })
      }
      
      if (this.parentId) {
        this.hasParent = true;
        this.db.getFolder(this.parentId).then(folder => {
          this.loadedNote.folder = folder;
          this.parentFolder = folder;
        })
      } else {
        this.hasParent = false;
      }
      this.db.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.db.observeFolders().subscribe(folders => {
            this.folders = folders;
          })
        } else {
        }
      })
    });
  }
    
  getFolder3DepsMax(maxDeep: number) {
    let folderChild = [];
    if (this.hasParent && this.folders && this.folders.length > 0) {
      folderChild = [_.find(this.folders, x => x.id == this.parentId)];
      for (let folder of folderChild) {
        folderChild.push(...(_.filter(this.folders, x => x.parentFolder == folder.id )));
      }
    } else if (this.folders && this.folders.length > 0) {
      folderChild = [{id: -1, name: 'No parent', deep: 0},...this.folders];
    }
    return _.filter(folderChild, x => x && x.deep < maxDeep);
  }

  openMenu() {
    this.menu.enable(true, 'first');
    this.menu.open();
  }

  public addItem() {
    if (this.currentNewItem.length > 0 && this.currentNewItem.length < 151) {
      this.errors.listItemSize = false;
      this.listItems.push(
        {
          item: this.currentNewItem,
          done: false
        }
      )
      this.currentNewItem = '';
    } else {
      this.errors.listItemSize = true;
    }
  }

  public removeItem(value: string) {
    let index = this.listItems.findIndex(x => x.item == value)
     this.listItems.splice(index, 1);
  }

  truncateString(str, num) {
    if (str.length <= num) {
      return str
    }
    return str.slice(0, num) + '...'
  }

  async addNote() {
    this.validateContentNote();
    this.validateTitleNote();
    if (!this.errors.titleSize && !this.errors.titleUnique && !this.errors.contentSize) {
      if (this.isEditing) {
        this.db.updateNote(this.loadedNote, JSON.stringify(this.listItems), this.loadedImage).then(data => {
          if (data == 0) {
            this.errors.titleUnique = true;
            return;
          }
        })
      } else {
        this.db.addNote(this.loadedNote.title, this.loadedNote.content, this.loadedNote.folder ? this.loadedNote.folder.id : '', this.loadedImage, JSON.stringify(this.listItems)).then(data => {
          if (data == 0) {
            this.errors.titleUnique = true;
            return;
          }
        })
      }
      this.loadedNote.title = '';
      this.loadedNote.content = '';
      this.loadedNote.listItems = [];
      this.currentNewItem = '';
      this.loadedImage = 'undefined'
      this.location.back();
      const toast = await this.toastController.create({
        message: 'Note created.',
        duration: 1000
      });
      toast.present();
    }
  }
  validateTitleNote() {
    this.errors.titleUnique = false;
    this.errors.titleSize = (this.loadedNote.title.length < 1 || this.loadedNote.title.length > 20);
  }

  validateContentNote() {
    this.errors.contentSize = (this.loadedNote.content.length < 1 || this.loadedNote.content.length > 500);
  }

  async addImage() {
    let options: ImagePickerOptions = {
      maximumImagesCount: 1,
      outputType: OutputType.FILE_URL,
      quality: 100,
    }
    await this.imagePicker.getPictures(options).then((results) => {
      this.loadedImage = results.length > 0 ? Capacitor.convertFileSrc(results[0]) : 'undefined';
    }, (err) => { 
      console.log('error picker', err)
     });
  }

  async useCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    await this.camera.getPicture(options).then((imageData) => {
     this.loadedImage = imageData ? Capacitor.convertFileSrc(imageData) : 'undefined';
    }, (err) => {
      console.log('error camera', err)
    });
  }

  clearImage() {
    this.loadedImage = '';
  }
}
