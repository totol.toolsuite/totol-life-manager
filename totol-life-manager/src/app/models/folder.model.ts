import { note } from "./note.model";

export class folder {
    constructor(){
        this.notes = [];
        this.folders = [];
        this.toDelete = false;
        this.onDelete = false;
    }
    id: number;
    name: string;
    icon: string;
    color: string;
    notes: note[];
    folders: folder[];
    parentFolder: folder;
    toDelete: boolean;
    isDeployed: boolean;
    deep: number;
    onDelete: boolean;
}