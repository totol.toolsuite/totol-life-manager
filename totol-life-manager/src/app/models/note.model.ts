import { folder } from "./folder.model";

export class note {
    constructor(){
        this.title = "";
        this.content = "";
    }

    id: number;
    title: string;
    content: string;
    folderId: number;
    folder: folder;
    imageUrl: string;
    listItems: any[];
    onDelete: boolean = false;
}