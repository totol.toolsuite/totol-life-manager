import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    loadChildren: () => import('./notes/list/list.module').then( m => m.ListPageModule)
  },
  {
    path: 'list/:folderId',
    loadChildren: () => import('./notes/list/list.module').then( m => m.ListPageModule)
  },
  {
    path: 'icon',
    loadChildren: () => import('./modal/icon/icon.module').then( m => m.IconPageModule)
  },
  {
    path: 'show/:noteId',
    loadChildren: () => import('./notes/show/show.module').then( m => m.ShowPageModule)
  },
  {
    path: 'show/:noteId/:root',
    loadChildren: () => import('./notes/show/show.module').then( m => m.ShowPageModule)
  },
  {
    path: 'show/:noteId/:root/:nest1',
    loadChildren: () => import('./notes/show/show.module').then( m => m.ShowPageModule)
  },
  {
    path: 'show/:noteId/:root/:nest1/:nest2',
    loadChildren: () => import('./notes/show/show.module').then( m => m.ShowPageModule)
  },
  {
    path: 'show/:noteId/:root/:nest1/:nest2/:nest3',
    loadChildren: () => import('./notes/show/show.module').then( m => m.ShowPageModule)
  },
  {
    path: 'folder',
    loadChildren: () => import('./new/folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'folder/:parentId',
    loadChildren: () => import('./new/folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'folder/edit/:parentId',
    loadChildren: () => import('./new/folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'note',
    loadChildren: () => import('./new/note/note.module').then( m => m.NotePageModule)
  },  
  {
    path: 'note/edit/:editedNote',
    loadChildren: () => import('./new/note/note.module').then( m => m.NotePageModule)
  },
  {
    path: 'note/:parentId',
    loadChildren: () => import('./new/note/note.module').then( m => m.NotePageModule)
  },
  {
    path: 'color',
    loadChildren: () => import('./modal/color/color.module').then( m => m.ColorPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
